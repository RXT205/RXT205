//#!/usr/bin/env openscad
/// Title: Extrusion Ancor
/// Description:

//@ Copyright (C) All Rights Reserved - Jacob Hrbek <kreyren@rixotstudio.cz> in 19/12/2021 00:10 CEST

ancor_x = 4.5;
ancor_y = 0.1;
ancor_z = 9;
ancor_handle = true;
ancor_optimize_material = true;

// NOTE-BUG(Krey): On OpenSCAD 2021.01 I had to add `0.1` to difference shapes to avoid rendering issues, remove once this is fixed upstream
    // - Decided to undo the 0.1 as it makes the code ugly af and added version-gate

// NOTE-BUG(Krey): On my version of OpenSCAD (01.2021) I have to use `{#` to maintain syntax highlighting for the contents, remove once this is fixed upstream
    // - Krey: Submitted https://github.com/openscad/openscad/issues/4016

// NOTE-BUG(Krey): The OpenSCAD 2021.01 does not ignore shebang so it ourputs an annoying error.. and refuses to run -> Undo the comment once this is fixed upstream
    // - Krey: Submitted https://github.com/openscad/openscad/issues/4015

// NOTE-WORKAROUND(Krey): Added workarounds to highlighted issues, remove once these issues are fixed by upstream in a stable release + 2 months and add assert for a version check

// DNR-TODO(Krey):
// - [X] Design is sufficient to ancor to the Ender 3
// - [X] Bugs were submitted and referenced
// - [X] Peer-review
// - [X] Problematic blocks version-gated and scheduled for removal
// - [ ] Optimized for material on demand
// - [ ] 2nd peer-review



///! Module designed to be ancored on the alluminium extrusion blocks.
///! Originally designed to ancor on Ender 3's extrusion to support the Z axis at the top
module ancor(x, y, z) {
	cube_size = z/x*1.15;

		difference() {
			cube([x, y, z], center = true);

			// Remove top left corner
			//translate([-x/4,0,z/2-cube_size/2]) {#
			translate([-x/2+cube_size/2, 0, z/2-cube_size/2]) {
				// Workaround(Krey): Workaround to OpenSCAD 2021.01 issues
				cube([cube_size+0.01, y+0.01, cube_size+0.01], center = true);
			}

			// Remove bottom left corner
			translate([-x/2+cube_size/2, 0, -z/2+cube_size/2]) {
				// Workaround(Krey): Workaround to OpenSCAD 2021.01 issues
				cube([cube_size+0.01, y+0.01, cube_size+0.01], center = true);
			}
		}

	/// Handle
	if (ancor_handle == true) {
		translate([x/2+x/2,0,0]) {
			cube([x, y, x*0.80], center = true);
		}
	};
}


// Create the ancor
//ancor(ancor_x, ancor_y, ancor_z);

///! Module defining v-slot profile extrusion(s)
module v_slot_profile(type, x, y, z) {
	difference() {
		difference() {
			square(x, center = true);
			circle(x/10);
		};

		translate([x*0.5, 0, 0]) {
			 #circle(5, $fn=4);
		}
	};
}

v_slot_profile(2020, 20, 0, 0);
