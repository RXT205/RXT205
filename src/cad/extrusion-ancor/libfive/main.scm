(set-bounds! [-10 -10 -10] [10 10 10])
(set-quality! 8)
(set-resolution! 1)

;; NOTE(Krey): This is already merged upstream, remove when able
  (define cube-centered box-mitred-centered)
;; NOTE(Krey): Make `(cube)` to display `(cube [0 0 0] [1 1 1])`

(define (v_slot_rail x y z)
  "libfive's shape defining V-Slot rail often used as a shape for common alluminium extrusion/profile"
    (let* (
      (core 7.65)
      (hole 2.15)
      (champer 6.4)
      (z-lenght 0.01))
        (difference
          ;;; Core
          (cube-centered [x x 0.01] [0 0 0])
          (cylinder hole 1 [0 0 (/ z -2)])
          
          ;;; Champer
          ;; FIXME-QA(Krey): For loop this
          (cube-centered [champer (- x core) z-lenght] [0 (/ x 2) 0])
          (cube-centered [champer (- x core) z-lenght] [0 (/ x -2) 0])
          (cube-centered [(- x core) champer z-lenght] [(/ x 2) 0 0])
          (cube-centered [(- x core) champer z-lenght] [(/ x -2) 0 0])
        )
    )
)
  
(v_slot_rail 19.9 19.9 0.5)

;;(extrude-z (rectangle [0 0] [5 5]) 0 0.01)

;;(extrude-z (polygon 1 4 [0 0]) 0 0.01)

;; AFFECTED_BY(Krey): 2D objects need additional bloat to work https://github.com/libfive/libfive/issues/480
;; OBSTRUCTED(Krey): Can't work in rixmacs (Emacs) https://github.com/libfive/libfive/issues/477
